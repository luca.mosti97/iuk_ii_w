package controllers;

import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Result;
import play.mvc.Controller;
import play.mvc.Result;
import services.BookService;
import services.NYTimesService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static play.mvc.Results.ok;

public class BestsellerController {

    private final NYTimesService nyTimesService;

    @Inject
    public BestsellerController(NYTimesService nyTimesService, HttpExecutionContext ec) {this.nyTimesService = nyTimesService;}

    public CompletionStage<Result> bestseller() {
        return nyTimesService.bestseller().thenApplyAsync(bestseller ->
                ok(Json.toJson(bestseller)));
    }
}
