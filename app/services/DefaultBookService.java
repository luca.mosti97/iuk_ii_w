package services;

import models.Book;
import repository.BookRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class DefaultBookService implements BookService{

    private BookRepository bookRepository;

    @Inject
    public DefaultBookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book getDummy() {
        final Book book = new Book();
        book.setId(1l);
        book.setDescription("Dieses Lehrbuch bietet eine umfassende Einführung in Grundlagen und Methoden der Computerlinguistik und stellt die wichtigsten Anwendungsgebiete in der Sprachtechnologie vor. Es richtet sich gleichermaßen an Studierende der Computerlinguistik und verwandter Fächer mit Bezug zur Verarbeitung natürlicher Sprache wie an Entwickler sprachverarbeitender Systeme.");
        book.setIsbn10("3827420237");
        book.setIsbn13("978-3827420237");
        book.setPages(736);
        book.setPublisher("Spektrum Akademischer Verlag");
        book.setTitle("Computerlinguistik und Sprachtechnologie: Eine Einführung (German Edition)");
        return book;
    }

    public CompletionStage<Stream<Book>> get() {
        return bookRepository.list();
    }

    public CompletionStage<Book> get(final Long id) {
        return bookRepository.find(id);
    }

    public CompletionStage<Boolean> delete(final Long id) {
        return bookRepository.remove(id);
    }

    public CompletionStage<Book> update(final Book book) {
        return bookRepository.update(book);
    }

    public CompletionStage<Book> add(final Book book) {
        return bookRepository.add(book);
    }

}